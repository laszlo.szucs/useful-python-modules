#
# magfluxconv package
#
# This python package converts flux (Jy) to magnitude and vica versa given
# the filter name or the zero magnitude flux of the filter.
# The code contains a database of common filters. Some of the zero fluxes and 
# related values are adopted from [http://www.adamgginsburg.com/filtersets.htm].
# the other values are from the instrument websites.
# 
# Copyright (C) 2018 Laszlo Szucs <laszlo.szucs@mpe.mpg.de>
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; version 2 of the License.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

import numpy as np

filter_systems = {
    # Johnson filter system, Bessel (1979)
    'U':   {'F0':1810.0,'dF0':0.0,'lam0':0.360,'dlam':0.150,'comment':'Johnson-Cousins U'}, \
    'B':   {'F0':4260.0,'dF0':0.0,'lam0':0.440,'dlam':0.220,'comment':'Johnson-Cousins B'}, \
    'V':   {'F0':3640.0,'dF0':0.0,'lam0':0.550,'dlam':0.160,'comment':'Johnson-Cousins V'}, \
    'R':   {'F0':3080.0,'dF0':0.0,'lam0':0.640,'dlam':0.230,'comment':'Johnson-Cousins R'}, \
    'I':   {'F0':2550.0,'dF0':0.0,'lam0':0.790,'dlam':0.190,'comment':'Johnson-Cousins I'}, \
    # Gunn griz system, Schneider, Gunn, & Hoessel (1983) 
    'g':   {'F0':3730.0,'dF0':0.0,'lam0':0.520,'dlam':0.140,'comment':'Gunn g'}, \
    'r':   {'F0':4490.0,'dF0':0.0,'lam0':0.670,'dlam':0.140,'comment':'Gunn r'}, \
    'i':   {'F0':4760.0,'dF0':0.0,'lam0':0.790,'dlam':0.160,'comment':'Gunn i'}, \
    'z':   {'F0':4810.0,'dF0':0.0,'lam0':0.910,'dlam':0.130,'comment':'Gunn z'}, \
    # 2MASS filters             
    'J':   {'F0':1594.0,'dF0':27.8,'lam0':1.235,'dlam':0.162,'comment':'2MASS J filter'}, \
    'H' :  {'F0':1024.0,'dF0':20.0,'lam0':1.662,'dlam':0.251,'comment':'2MASS H filter'}, \
    'Ks':  {'F0':666.70,'dF0':12.6,'lam0':2.159,'dlam':0.262,'comment':'2MASS Ks filter'}, \
    # WISE bands
    'W1':  {'F0':309.540,'dF0':4.582,'lam0':3.4, 'dlam':0.625,'comment':'WISE [3.4]'}, \
    'W2':  {'F0':171.787,'dF0':2.516,'lam0':4.6, 'dlam':1.042,'comment':'WISE [4.6]'}, \
    'W3':  {'F0':31.674, 'dF0':0.450,'lam0':12.0,'dlam':5.506,'comment':'WISE [12]'}, \
    'W4':  {'F0':8.363,  'dF0':0.124,'lam0':22.0,'dlam':4.101,'comment':'WISE [22]'}, \
    # Spitzer IRAC and MIPS
    'I3.6':{'F0':280.9,'dF0':4.1,'lam0':3.550,'dlam':0.676,'comment':'Spitzer IRAC [3.6]'}, \
    'I4.5':{'F0':179.7,'dF0':2.6,'lam0':4.493,'dlam':0.731,'comment':'Spitzer IRAC [4.5]'}, \
    'I5.8':{'F0':115.0,'dF0':1.7,'lam0':5.731,'dlam':0.589,'comment':'Spitzer IRAC [5.8]'}, \
    'I8':  {'F0':64.13,'dF0':.94,'lam0':7.872,'dlam':0.556,'comment':'Spitzer IRAC [8]'}, \
    'MI24':{'F0':7.170,'dF0':.11,'lam0':23.675,'dlam':9.126,'comment':'Spitzer MIPS [24]'}, \
    'MI70':{'F0':0.778,'dF0':.0119,'lam0':71.42,'dlam':19.653,'comment':'Spitzer MIPS [70]'}, \
    'MI160':{'F0':.160,'dF0':.00245,'lam0':155.9,'dlam':48.356,'comment':'Spitzer MIPS [160]'}, \
                }

def getF0(band):
    '''
    If band (str type) is in filter_systems then returns the zero magnitude 
    flux and zero magnitude flux error otherwise it raises error.
    '''
    global filter_systems
    if type(band) != str:
        raise ValueError("Non string band argument given in InFilterSystem().")
        
    if band in filter_systems.keys():
        Fzeromag  = filter_systems[band]['F0']
        Fzero_err = filter_systems[band]['dF0']
    else:
        raise ValueError('Band name {s} is unkown, please supply F0'.format(band))
    
    return (Fzeromag,Fzero_err)

def show_filters():
    '''
    Prints the list of filters/bands in the database.
    '''
    global filter_systems
    print
    print " Name  | wavelength | zero flux  | comment "
    print
    for flt in filter_systems.keys():
        print " {:5s} | {:10.2f} | {:10.2f} | {:s} ".format( 
            flt,
            filter_systems[flt]['lam0'],
            filter_systems[flt]['F0'],
            filter_systems[flt]['comment'] )
    return 

def mag2flux(mag, band, F0=None):
    '''
    Converts magnitude value to flux density (Jy = 1.0E-23 erg/s/cm**2/Hz).
    
    Parameters:
    
      mag       ==  magnitude value
      band      ==  measurement band or filter
      F0        ==  zero magnitude flux (Vega flux) in the band (optional, use for
                    uncommon bands. If set then band argument is disregarded.

    Returns:
    
      F         ==  flux density (Jy) equivalent of magnitude value
    '''
    # Determine F0
    if F0:
        Fzeromag  = F0
        Fzero_err = 0.0
    else:
        if (type(band) == str):
            Fzeromag, Fzero_err = getF0(band)
            
        elif (type(band) in [list,np.ndarray]):
            Fzeromag  = np.zeros_like(band,dtype=np.double)
            Fzero_err = np.zeros_like(band,dtype=np.double)
            for i in range(len(band)):
                Fzeromag[i], Fzero_err[i] = getF0(band[i])

    # Array or scalar?
    if ( np.shape(mag) == np.shape(Fzeromag) ) or (np.shape(Fzeromag) == () ):
        F = 10.0**( np.array(mag) / -2.5 ) * Fzeromag
    else:   
        raise ValueError("Incorrect input array length.")
        
    # Return result
    return F

def flux2mag(F, band, F0=None):
    '''
    Converts flux density (Jy = 1.0E-23 erg/s/cm**2/Hz) to photometric magnitude.
    
    Parameters:
    
      F         ==  flux density value (in Jy)
      band      ==  measurement band or filter
      F0        ==  zero magnitude flux (Vega flux) in the band (optional, use for
                    uncommon bands. If set then band argument is disregarded.

    Returns:
    
      mag       ==  magnitude equivalent of the flux density F.
    '''
    # Determine F0
    if F0:
        Fzeromag  = F0
        Fzero_err = 0.0
    else:
        if (type(band) == str):
            Fzeromag, Fzero_err = getF0(band)
            
        elif (type(band) in [list,np.ndarray]):
            Fzeromag  = np.zeros_like(band,dtype=np.double)
            Fzero_err = np.zeros_like(band,dtype=np.double)
            for i in range(len(band)):
                Fzeromag[i], Fzero_err[i] = getF0(band[i])

    # Array or scalar?
    if ( np.shape(F) == np.shape(Fzeromag) ) or (np.shape(Fzeromag) == () ):
        mag = -2.5 * np.log10(F / Fzeromag)
    else:   
        raise ValueError("Incorrect input array length.")
        
    # Return result
    return mag
