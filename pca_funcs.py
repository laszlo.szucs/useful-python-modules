'''
Perform Principal component analysis on column density maps.

The code reads in fits files from a folder and uses sklearn library to 
determine the principal components of the map set. If physical conditions 
are supplied then the code attempts to match the principal components with 
the physical conditions.

For more information see: 
http://scikit-learn.org/stable/modules/decomposition.html#pca

The code is written by Laszlo Szucs (laszlo.szucs@mpe.mpg.de) and licensed 
under the LGPL license.

The code is based on pca_standardization.py by S. Spezzano (2015).

Example:

  import pca_funcs
  import glob
  fitslist = glob.glob('*.fits')
  phys     = glob.glob('phys/*.fits')
  pcadata  = pca_funcs.run_pca(fitslist, physlist=phys, ncomp=8, show=True)

'''

import matplotlib.pyplot as plt
from astropy.io import fits
import numpy as np
from sklearn import decomposition
from sklearn import preprocessing
from scipy import stats

def run_pca(fitslist, ncomp=None, physlist=None, physname=None, 
            verbose=False, show=False, maps=True, pcas=True, pcax=True):
    '''
    Performs PCA analysis on a list of fits files.
    
    The fits files should be two dimensional with equal sizes. The fits header
    should contain 'SPEC' string tag.
    
    If physlist argument is set, then the code compares the principal component 
    maps to physical component maps using Spearman R statistic.
    
    Input
    -----
       fitslist   list containing fits file full path and names
       ncomp      number of fitted principal components. If not set then 
                  ncomp = len(fitslist)-1 is used.
       physlist   list containing full path and file name of physical condition 
                  fits maps.
       physname   list of physical condition names. If not set then file names 
                  are used.
       verbose    print details and information
       show       if True then shows figures on screen
       maps       if True then plot input column density maps
       pcas       if True then plot principal component maps
       pcax       if True then plot column density map variance along the principal
                  component axis
       
    Returns
    -------
       pca_data   dictionary of input maps, principal components and if 
                  physlist set then comparison with physical conditions. See 
                  compute_pca() for more details.
    '''

    pca_data = compute_pca(fitslist, ncomp=ncomp, verbose=verbose)
    
    # Plot if switch set true
    if maps:
        plot_maps(pca_data, show=False)
    if pcas:
        plot_pcs(pca_data, show=False)
    if pcax:
        plot_pca_corr(pca_data, show=False)        
    if physlist:
        get_phys_maps(physlist, pca_data, physname=physname)
    
    if show:
        plt.show()
        
    return pca_data


def compute_pca(fitslist, ncomp=None, verbose=False):
    '''
    Prepare input maps and perform PCA.
    
    Input
    -----
       fitslist   list containing fits file full path and names
       ncomp      number of fitted principal components. If not set then 
                  ncomp = len(fitslist)-1 is used.
       verbose    print details and information
       
    Returns
    -------
       pca_data   dictionary of input maps, principal components
                  pca_comp -  PC components
                  pca_sco  -  explained variance ratio
                  pca_map  -  reconstructed PC maps
                  pca_lin  -  linearised PC maps
                  imdata   -  column density maps
                  imspec   -  species name of col. dens. maps
    '''
    
    nfits = len(fitslist)
    if not ncomp:
        ncomp = nfits - 1
    
    # Call preprocess_maps()
    prep_map = preprocess_maps( fitslist, verbose=verbose )
    
    # Compose PCA
    pca = decomposition.PCA(n_components=ncomp)
    pca.fit( prep_map['improc'] )
    
    pca_sco = pca.explained_variance_ratio_
    
    pca_comp = pca.components_
    
    # Construct principal component maps
    pca_lin = []
    pca_map = []
    for i in range(ncomp):
        data_lin_re = prep_map['improc'][:, 0] * 0.0
        for j in range(nfits):
            data_lin_re = data_lin_re + pca_comp[i,j] * prep_map['improc'][:,j]
        pca_lin.append( data_lin_re )
        pca_map.append( np.reshape( data_lin_re, prep_map['shape']) )
        
    pca_lin = np.asarray(pca_lin)
    pca_lin = pca_lin.transpose()
    pca_map = np.asarray(pca_map)
    pca_map = pca_map.transpose()
    
    #
    pca_data = {'pca_comp': pca_comp, 'pca_sco': pca_sco, 
                'pca_map': pca_map, 'pca_lin': pca_lin , 
                'imdata': prep_map['imdata'], 
                'imspec': prep_map['imspec']}
    
    return pca_data

def preprocess_maps(fitslist, verbose=False):
    '''
    Pre-processes fits files for PCA
    
    The function reads in the files specified in the argument (list), 
    calls renorm_flatten() function on each and saves the flattened and 
    normalised data to len(fitslist) x len(image_pixel) ndarray object.
    
    Input
    -----
       fitslist   list containing fits file full path and names
       verbose    print details and information
       
    Returns
    -------
       prep_map   dictionary containing the original (imdata) and preprocessed 
                  (improc) images and the corresponding species name (imspec). 
                  The input fits shape is also saved to the dictionary.
    '''

    nfits  = len(fitslist)
    imdata = []
    improc = []
    imspec = []
    
    fits_shape = None
    
    for f in fitslist:
        try:
            hdu = fits.open(f)
        except:
            raise ValueError('ERROR: File in list not found: {}'.format(f))
        # Read data
        imdata.append(hdu[0].data)
        fits_shape = hdu[0].data.shape
        imspec.append(hdu[0].header['SPEC'])
        improc.append(renorm_flatten(hdu[0].data))
        hdu.close()

    imdata = np.asarray(imdata)
    imdata = imdata.transpose()
    improc = np.asarray(improc)
    improc = improc.transpose()
    improc = improc[0,:,:]

    if verbose:
        print "INFO: preprocessed array shape: {}".format(improc.shape())

    #
    prep_map =  {'improc': improc, 'imspec': imspec, 
                 'imdata': imdata, 'shape': fits_shape}

    return prep_map

def renorm_flatten(twodarray):
    '''
    Renormalise and flatten 2 dimensional array
    
    This is used for preparing fits images for PCA.
    
    Input
    -----
       twodarray    two dimensional ndarray to be renormalised and flattened.
    
    Returns
    -------
       flatnorm     one dimensional normalised ndarray, created from input.
    '''
    
    twodarray = np.double(twodarray)
    flat = np.reshape( twodarray, (np.product(twodarray.shape), 1) )
    flatnorm = preprocessing.StandardScaler().fit_transform(flat)
    
    return flatnorm

def plot_pcs(pca_data, ncol=2, cmap='CMRmap_r', extent=[-0.7,0.7,-0.7,0.7],
             saveFig=False, figsize=(8,12), show=True):
    '''
    Plot principal component maps computed by the compute_pca() function.
    
    Input
    -----
       pca_data   dictionary output from run_pca() function
       ncol       figures will be plotted in this number of columns
       cmap       colour map of the plot (default: CMRmap_r)
       extent     physical extent of the map (in parsec)
       saveFig    if true or string (figure name) then the figure is saved to disk.
       
    Returns
    -------
       Multi panel figure showing the principal component maps.
    '''
    im_shape = pca_data['pca_map'].shape[0:2]
    nmap = pca_data['pca_map'].shape[2]
    
    fig = plt.figure()
    
    nrow = nmap / ncol
    if nmap%ncol > 0:
        nrow = nrow + 1

    # Find H2 column density if present in data
    try:
        iH2 = pca_data['imspec'].index('H2')
        H2map = np.log10(pca_data['imdata'][:,:,iH2])
    except:
        H2map = None

    for i in range(nmap):
        ax = plt.subplot(nrow, ncol, i+1)
        show_map(pca_data['pca_map'][:,:,i], fig, ax, H2map=H2map, 
                 title=("PC" + str(i+1) + " " + 
                        str(round(pca_data['pca_sco'][i]*100., 1)) + "%"), 
                 cmap=cmap, extent=extent)

    fig.set_size_inches(figsize, forward=True)
    fig.canvas.set_window_title('Principal component maps')
    plt.tight_layout()

    if show:
        plt.show()
    
    return

def plot_maps(data, ncol=8, cmap='CMRmap_r', extent=[-0.7, 0.7, -0.7, 0.7],
              saveFig=False, figsize=(20,12), show=True):
    '''
    Plots column density maps from either run_pca() or preprocess_maps()
    output dictionary.
    
    Input
    -----
       data       dictionary output from run_pca() function
       ncol       figures will be plotted in this number of columns
       cmap       colour map of the plot (default: CMRmap_r)
       extent     physical extent of the map (in parsec)
       saveFig    if true or string (figure name) then the figure is saved to disk.
       
    Result
    ------
       Multi panel figure showing the principal component maps.
    '''
    im_shape = data['imdata'].shape[0:2]
    nmap = data['imdata'].shape[2]
    
    maps = data['imdata']
    maps = data['imdata']
    
    fig = plt.figure()
    
    nrow = nmap / ncol
    if nmap%ncol > 0:
        nrow = nrow + 1
    
    # Find H2 column density if present in data
    try:
        iH2 = data['imspec'].index('H2')
        H2map = np.log10(data['imdata'][:,:,iH2])
    except:
        H2map = None
    
    for i in range(nmap):
        ax = plt.subplot(nrow, ncol, i+1)
        show_map(data['imdata'][:,:,i], fig, ax, H2map=H2map, 
                 title=data['imspec'][i], cmap=cmap, extent=extent)
        
    fig.set_size_inches(figsize, forward=True)
    fig.canvas.set_window_title('Column density maps')
    plt.tight_layout()
    
    if show:
        plt.show()
    
    return


def show_map(map_data, fig, ax, H2map=None, title=None, cmap='CMRmap_r', 
             extent=[-0.7,0.7,-0.7,0.7]):
    '''
    Show / plot a single map data.
    
    Input
    -----
       map_data  2D array to be plotted
       ax        axis where the map is plotted, if not set then create new figure.
       title     map title
       cmap      colour map of the plot (default: CMRmap_r)
       extent    physical extent of the map (in parsec)
    '''

    if not fig:
        fig = plt.figure()
    if not ax:
        ax = plt.subplot(1, 1, 0)

    im=ax.imshow(map_data.transpose(), cmap=cmap, extent=extent, 
                     origin='lower')
    if H2map.any():
        h2cont = ax.contour(H2map.transpose(), colors='k', origin='lower', 
                            extent=extent, linewidths=0.75)
        plt.clabel(h2cont, inline=1, fontsize=5, manual=False)
    
    ax.set_title(title)
    fig.colorbar(im)
    
    return

def plot_pca_corr(pca_data, ylim=None, figsize=(8,16), show=True):
    '''
    Show / plot the principal axis in feature (column density map) space.
    This gives the variance of the map along the principal axis: higher variance 
    the more correlation between map and principal component.
    
    Input
    -----
       pca_data   dictionary output from run_pca() function
       ylim
       figsize
       show
    '''
    npc, nsp = pca_data['pca_comp'].shape
    
    fig = plt.figure()
    width = 1
    xes = np.arange(nsp)

    for i in range(npc):
        ax = plt.subplot(npc, 1, i+1)
        ax.bar(xes+0.5, pca_data['pca_comp'][i,:], width=1, edgecolor='none', 
               alpha=0.5, color = 'k')
        # set axis limits
        if not ylim:
            ylow  = max([pca_data['pca_comp'][i,:].min(),-1])
            yhigh = min([pca_data['pca_comp'][i,:].max(),1])
        else:
            ylow, yhigh = ylim
        ax.set_ylim(ylow,yhigh)
        ax.set_xlim(0, nsp)
        ax.set_xticks(xes)
        ax.grid(True)
        ax.set_title("PC" + str(i+1),x=0.05)
        ax.set_xticklabels(pca_data['imspec'], rotation='vertical', ha='left')

    fig.set_size_inches(figsize, forward=True)
    fig.canvas.set_window_title('Variance in feature space along principal axis')
    plt.tight_layout()

    if show:
        plt.show()
    
    return

def match_pca_phys(physmap, pca_data, physname=None, verbose=True):
    '''
    Computes correlation between principal component maps and physical condition 
    maps.
    
    The maps are compared using the Spearman R test (scipy.stats.spearmanr).
    '''
    pca_shape = pca_data['pca_map'].shape[0:2]
    npca = pca_data['pca_map'].shape[2]
    
    phys_shape = physmap.shape
    
    if phys_shape != pca_shape:
        raise ValueError('ERROR: physmap shape {} does not match with PC map \
                         shape {}'.format(physmap.shape,im_shape))
    
    phys_lin = np.reshape( physmap, (np.product(phys_shape),1) )
    
    # Compute correlation with each PC components
    corr = np.zeros(npca)
    pval = np.zeros(npca)
    for i in range(npca):
        corr[i], pval[i] = stats.spearmanr(pca_data['pca_lin'][:,i], phys_lin)
        
    # Sort results by probability
    srt = corr.argsort()
    if verbose:
        if physname:
            print "\nMatching <{}> with PCs:\n".format(physname)
        for i in range(npca):
            j = srt[i]
            print "PC{}: correlation: {:.3f}  p-value: {:.3f}".format(j+1, 
                                                                      corr[j], 
                                                                      pval[j])
            
    return {'physmap': physmap, 'physname': physname, 
            'corr': corr, 'pval': pval}

def get_phys_maps(fitslist, pca_data, physname=None):
    '''
    Reads in physical condition data and calls match_pca_phys() to compute 
    their correlation with the principal components in pca_data.
    
    Input
    -----
       fitslist    list containing the physical condition fits file locations
       pca_data    dictionary with results from run_pca()
       physname    name of physical conditions listed in fitslist. If not given
                   then filename is used.
                   
    Result
    ------
       Computes the Spearman R correlation between the specified physical 
       conditions and the principal component maps.
    '''
    nfits  = len(fitslist)
    physdata = []
    if not physname:
        physname_ = []
    else:
        physname_ = physname
    
    for f in fitslist:
        try:
            hdu = fits.open(f)
        except:
            raise ValueError('ERROR: File in list not found: {}'.format(f))
        # Read data
        physdata.append(hdu[0].data)
        if not physname:
            try:
                physname_.append(hdu[0].header['SPEC'])
            except:
                print f
                physname_.append(f)
        hdu.close()
        
    for i in range(nfits):
        a = match_pca_phys(physdata[i], pca_data, physname=physname_[i], 
                           verbose=True)
        
    return
