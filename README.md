Intorduction
------------

This is a collection of small and often unrelated python modules with 
astrophyisical use cases.


Contents
--------

#### fluxmagconv.py

Module to convert flux densities to magnitude and vice versa given 
the zero magnitude flux or the band name. The module contains a 
database of common near/mid-infrared bands / filters.

   Usage: 
```python
import fluxmagconv
#
# Convert magnitude to flux
F_ubv = fluxmagconv.mag2flux( [3.,2.,1], ['U','B','V'] )
#
# Convert flux to magnitude
m_w1 = fluxmagconv.flux2mag( 342., 'W1' )
m_o  = fluxmagconv.flux2mag( 342., None, F0=2001. )
#
# Show filter database
fluxmagconv.show_filters()
```

#### pca_funcs.py

Module to perform principal component analysis on a set of column density 
maps. The maps should all have the same resolution and be stored in fits 
files. The fits header should contains a 'SPEC' field, indicating the 
corresponding species name.

Optionally, the principal components are compared to physical condition maps 
using Spearman's rank correlation coefficient.

   Usage: 
```python
import glob
import pca_funcs
#
# Search for column density and physical condition maps
fitslist = glob.glob( '*.fits' )
phys     = glob.glob( 'phys/*.fits' )
#
# Run the full test and show all figures
pcadata  = pca_funcs.run_pca( fitslist, physlist=phys, ncomp=8, show=True )
```

Installation
------------

Add the code folder to your $PYTHONPATH environment variable, e.g.

```bash
export PYTHONPATH = $PYTHONPATH:~/useful-python-modules
```

Add the above line to ~/.bashrc or ~/.profile to make it persistent. 
